## Notification №5 "Notifying the Giver that Presentee did not confirm (ignored) accepting of the gift."

**Example of the data structure in JSON that may be sent to developer's WebHook URL**

### Notification body
```json
  {
    "create_time": "2000-01-01 00:00:00 UTC",
    "event_type": "order_not_confirmed",
    "resource_type": "notification",
    "resource": {
      "for": "uniq_giver_id",
      "title": "short description",
      "message": "full description:\\nMake and receive real gifts with YouGiver!",
      "giver_name": "giver name",
      "recipient_name": "recipient name",
      "gift_request": "123456789"
    }
  }
```
