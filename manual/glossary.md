**Developer’s app** - the app that has been integrated / is being integrated with YouGiver.

**Giver** - a participant and user of  developer’s app that performs the gift giving from the YouGiver catalog through one of the interaction scenarios.

**Presentee** - the process participant who receives the gift from the Giver.

**Scenario** - a business process, according to which the Giver can perform the gift giving to the Recipient from the YouGiver catalog.

**Notification** - a message that YouGiver sends to developer’s app to transfer it further to  the Giver or Presentee. It is used for the completeness of the gift giving process and the  informing of the participating parties.