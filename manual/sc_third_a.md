## **3a Scenario description**

The gift giving process under 3a Scenario provides for:

1. the user who will act as the Giver is the user of Developer’s app;
2. the user who will act as the Presentee may not be a user of Developer’s app;
3. on Developer’s application side there is **NO** information about:
	- the country of residence of users who act as the Giver and the Presentee;
	- the city of residence of users who act as the Giver and the Presentee;
4. after the initialization of the 3a Scenario, the Giver will be able to specify all the necessary information for delivery of the gift to the Presentee.
  
### **3a Scenario initialization**

1. Send the POST request to `https://service.yougiver.me/api/v1/gift_requests` with the possible parameters:

<!--table-->

| Parameter                               | Description                                  |
|---------------------------------------  |------------------------------------------ |
| gift_request[developer_number] **\***   | Your developer_number in Yougiver service   |
| gift_request[giver_id] **\***           | Giver's ID in your service    |
| gift_request[giver_name]                | Giver's Name                              |
| gift_request[giver_email]               | Giver's Email                            |
| gift_request[giver_phone]               | Giver's Phone number                          |
| gift_request[giver_nickname]            | Giver's Nickname                          |
| gift_request[giver_gender]              | Giver's Gender                              |
<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Parameter | Description                                      |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

### **Supported event notifications in 3a Scenario**

1. **Event:** Change of order status to "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."

2. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."