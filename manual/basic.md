#### To integrate your app with YouGiver you need:
1. To go through registration and the Developer's profile moderation process on the [service](https://service.yougiver.me/sign/up). Then, you will have YouGiver API information available in your personal account.

2. To select the set of gift giving scenarios available for integration into your app. All available scenarios are listed below:
    - **1 Scenario.** You as a Developer always know the country and city of residence of your service’s users (this data should always be stored in your app and updated constantly) and you forward this data to YouGiveras in request.
    - **2а Scenario.** You as a Developer don't know the country and city of residence of your service’s users, but the Giver knows the location of the person (s)he would like to give a gift to. The Giver has to specify the location info of the Recipient by himself/herself after the launch of the scenario.
    - **2b Scenario.** You as a Developer don't know the country and city of residence of your service’s users, and  the Giver **DOES NOT** know the location of the person (s)he would like to give a gift to.  In this case, the link to the form will be sent to the Presentee to fill the gift delivery information.
    - **3a Scenario.** Your app’s user (the Giver) can send a gift to anyone (including users outside your app). The  Giver should know all the necessary data of the Presentee and enters them him-/herself after  the launch of the scenario.
    - **3b Scenario.** Your app’s user (the Giver) can give a gift to anyone (including users outside your app). The  Giver **DOES NOT** know the contact information of the Presentee. In this case, a link to the form will be sent to the Presentee (with the request to fill in the data for the gift delivery)  through any messenger available for the Giver (the Giver chooses it on their own).
    - **3c Scenario.** Your app’s user (the Giver) can give a gift to anyone (including users outside your app). **Giver knows the country and city of residence of the user who will act as the Presentee**. In this case, a link to the form will be sent to the Presentee (with the request to fill in the data for the gift delivery)  through any messenger available for the Giver (the Giver chooses it on their own).

3. In the Developer's personal account, create/set up WebHooks with the necessary event notifications for the interaction scenarios 
you are planning on integrating into your app.

4. Implement the initialization of the chosen Scenarios (using the **developer_number**) in your app according to the documentation concerning YouGiver scenarios (see below).

5. Test the correct work of the chosen Scenarios and connected notifications.

6. Launch integration in the production mode.