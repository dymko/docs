## **3c Scenario description**

The gift giving process under the Scenario 3c provides for:

1. the user who will act as the Giver is the user of the Developer’s app;
2. the user who will act as the Presentee is **NOT** a user of the Developer’s app;
3. on the Developer’s application side there is **NO** information about:
	- the country of residence of users who act as the Giver and the Presentee;
	- the city of residence of users who act as the Giver and the Presentee;
4. Giver knows the country and city of residence of the user who will act as the Presentee;
5. sending the link to the Presentee to the form to fill out the gift delivery information to any app installed on the device of the Giver (the Giver chooses it).


### **3c Scenario initialization**

1. Send the POST request to `https://service.yougiver.me/api/v1/gift_requests` with the possible parameters:

<!--table-->

| Parameter                               | Description                                  |
|---------------------------------------  |------------------------------------------ |
| gift_request[developer_number] **\***   | Your developer_number in the YouGiver service   |
| gift_request[giver_id] **\***           | Giver's ID in your service     |
| gift_request[giver_name]                | Giver's Name                              |
| gift_request[giver_email]               | Giver's Email                            |
| gift_request[giver_phone]               | Giver's Phone number                          |
| gift_request[giver_nickname]            | Giver's Nickname                          |
| gift_request[giver_gender]              | Giver's Gender                              |

<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Parameter | Description                                      |
|--------- |---------------------------------------------- |
| url      | Link that Giver has to follow |

<!--endtable-->

### **Supported event notifications in 3c Scenario**

1. **Event:** Filling out contact information by the Presentee → **Notification #2:** "The Giver's notification of the gift confirmation. Filling out the contact information by the Presentee."
2. **Event:** Change of order status to - "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
3. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
4. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **Notification #5:** "Notifying the Giver that the Presentee did not confirm (ignored) accepting of the gift."
5. **Event:** Payment for the gift by the Giver → **Notification #8**: "Notification for the Giver with a link to be shared with Presentee".